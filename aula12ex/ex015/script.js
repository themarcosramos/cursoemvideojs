function verificar(){
    var data = new Date()
    var ano = data.getFullYear()
    var fano = document.getElementById('txtano')
    var res = document.querySelector('div#res')
    var foto = document.querySelector('div#res')
    if (fano.value.length == 0 || fano.value > ano) {
        window.alert('[ERRO] Verifique os dados e tente novamente')
    } else{
        var fsex = document.getElementsByName('radsex')
        var idade= ano - Number(fano.value)
        var genero = ''
        var img = document.createElement('img')
        img.setAttribute('id','foto')
        if(fsex[0].checked ){
             //genero = 'Homem'
             if (idade >=0 && idade < 10) {
                // criança
                img.setAttribute('src','fotos/crianca-H.jpg')
                genero = 'uma criança'
            } else if (idade < 21) {
                // jovem
                img.setAttribute('src', 'fotos/jovem-H.jpg')
                genero = 'um jovem'
            } else if (idade < 50){
                // adulto
                img.setAttribute('src','fotos/adulto-H.jpg')
                genero = 'um adulto'
            } else {
                // idoso
                img.setAttribute('src','fotos/idoso-H.jpg')
                genero = 'um idoso'
            }

        }else if(fsex[1].checked){
             //genero = 'Mulher'
             if (idade >=0 && idade < 10) {
                // criança
                img.setAttribute('src','fotoscrianca-M.jpg')
                genero = 'uma criança'
            } else if (idade < 21) {
                // jovem
                img.setAttribute('src','fotos/jovem-M.jpg')
                genero = 'uma jovem'
            } else if (idade < 50){
                // adulto
                img.setAttribute('src','fotos/adulto-M.jpg')
                genero = 'uma adulta'
            } else {
                // idoso
                img.setAttribute('src','fotos/idoso-M.jpg')
                genero = 'uma idosa'

            }
        }
       res.style.textAlign = 'center'
       res.innerHTML = `Detectamos ${genero} com ${idade} anos`
       foto.appendChild(img)
       foto.style.textAlign='center'
       foto.style.margin ='20px'
   }

}