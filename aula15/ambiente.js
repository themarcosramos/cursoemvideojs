let num = [5,4,3,2,1]

console.log(num)

num[5] = 8

console.log(num)

num.push(7)

console.log(num)

console.log(num.length)

num.sort()

console.log(num)

console.log(num[0])

console.log("======================")

let c = num.length;
 
let i = 0

while (i<=c) {
    console.log(num[i])
    i++  
}

console.log("======================")

for (let a=0; a < num.length;a++){
       console.log(num[a])
}

console.log("======================")

for( let a in num ){
    console.log(num[a])
}

console.log("##########################")

let  y = num.indexOf(7)

console.log(y)

